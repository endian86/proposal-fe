一、说明

Api地址：http://45.77.8.117:7005

测试服务器位于日本，可能比较慢。

所有接口，GET方法参数放在query里。其他方法参数放在body里。

二、基础API

1. /v1/districts

描述

- 获取学区列表

请求方式

- GET

参数

   参数名      是否必须     类型        说明    
  offset     否      int     偏移量，用于分页 
  limit      否      int     返回个数，默认20



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     Array   学区列表

返回示例

    {
        "list": [
            {
                "districtId": 2,
                "districtName": "921011102ceshi11"
            },
            {
                "districtId": 6,
                "districtName": "test"
            }
        ],
        "reqId": "1931361002",
        "errCode": 0,
        "errMsg": "ok"
    }

2. /v1/schools

描述

- 获取学校列表

请求方式

- GET

参数

   参数名      是否必须     类型        说明    
  offset     否      int     偏移量，用于分页 
  limit      否      int     返回个数，默认20



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     Array   学校列表

返回示例

    {
        "list": [
            {
                "schoolId": 2,
                "schoolName": "北大附中"
            },
            {
                "schoolId": 3,
                "schoolName": "清华附中"
            }
        ],
        "reqId": "1931a81003",
        "errCode": 0,
        "errMsg": "ok"
    }



3. /v1/domains

描述

- 获取领域列表

请求方式

- GET

参数

   参数名      是否必须     类型        说明    
  offset     否      int     偏移量，用于分页 
  limit      否      int     返回个数，默认20



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     Array   领域列表

返回示例

    {
        "list": [
            {
                "domainId": 1,
                "domainName": "IT"
            }
        ],
        "reqId": "585e6f1002",
        "errCode": 0,
        "errMsg": "ok"
    }

4.  /v1/verification

描述

- 获取验证码

请求方式

- POST

参数

   参数名  是否必须      类型     说明 
  phone  是      string  手机号 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息

返回示例

    {
        "reqId": "585e6f1002",
        "errCode": 0,
        "errMsg": "ok"
    }



 三、学生相关API

1. /v1/student

描述

- 注册一个学生

请求方式

- POST

参数

     参数名    是否必须      类型     说明  
    phone    是      string  学生手机号
  validCode  是      String   验证码 
  password   是      String   密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "studentId": 2,
            "createTime": "2018-01-15T17:43:19.609Z"
        },
        "reqId": "79f8371001",
        "errCode": 0,
        "errMsg": "ok"
    }

2. /v1/student/token

描述

- 学生账号登录，返回token及学生个人信息

请求方式

- POST

参数

    参数名     是否必须      类型     说明  
   phone     是      String  学生手机号
  password   是      String   密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "studentId": 2,
            "name": null,
            "phone": "18661205610",
            "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg",//头像
            "educationId": null,//教育id
            "sex": 0,//性别 0-未知，1-男，2-女
            "birthday": "1900-01-01T00:00:00.000Z",//生日
            "District": {//学区信息
                "districtId": 6,
                "districtName": "921011102ceshi11"
            },
            "School": {//学校信息
                "SchoolId": 1,
                "SchoolName": "未知"
            },
            "grade": null,//年级
            "intro": null,//个人介绍
            "token": "f89c15683f81b4e6bfbd83c04598c310df5be6e60fc73aab21ebc392ac184bc90716ae7c4c049aab82f5c729d178dcacbe06fe0849a2cf5984493f3b6245f954",
            "expires_time": 1518631209131
        },
        "reqId": "dcfc291001",
        "errCode": 0,
        "errMsg": "ok"
    }

3. /v1/student

描述

- 修改一个学生个人信息

请求方式

- PUT

参数

      参数名       是否必须      类型        说明    
     token       是      string   鉴权token  
     name        否      string     学生姓名   
    avatar       否      string      头像    
  educationId    否      string     教育id   
      sex        否       Int     1-男，2-女  
   birthday      否       date       生日    
  districtId     否       Int       学区id   
   schoolId      否       Int       学校id   
     grade       否       int    应该是枚举@todo
     intro       否      String     个人介绍   



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "studentId": 2
        },
        "reqId": "bcf70c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

4. /v1/student/action_forget_password

描述

- 学生账号忘记密码

请求方式

- PUT

参数

     参数名    是否必须      类型     说明  
  validCode  是      string   验证码 
    phone    是      string  学生手机号
  password   是      string   新密码 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "phone": 18700102254
        },
        "reqId": "bcf70c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

5. /v1/student/action_update_password

描述

- 修改一个学生账号密码

请求方式

- PUT

参数

      参数名       是否必须      类型      说明   
     token       是      string  鉴权token
  oldPassword    是      string    旧密码  
  newPassword    是      string    新密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "phone": "18700102254"，
            "studentId": "2"
        },
        "reqId": "bcf70c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

6. /v1/student/action_update_phone

描述

- 修改一个学生账号的手机号

请求方式

- PUT

参数

     参数名    是否必须      类型      说明   
    token    是      string  鉴权token
  validCode  是      string    验证码  
    phone    是      string   新手机号  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {//更换手机号后原token失效，因此直接返回了新的token
            "phone": "18661205610",
            "token": "8e6256b8b39603a642e68270f73e7becdc92c44ae5036d43eef9e92417cd9e6c444cb5261ed1085fe8586a2dd0f9a4736edca32e3916f2746a5dac9fd9cf83a1",
            "expires_time": 1518694883140
        },
        "reqId": "bdf4e21003",
        "errCode": 0,
        "errMsg": "ok"
    }

7. /v1/student

描述

- 获取一个学生账号个人信息

请求方式

- GET

参数

   参数名  是否必须      类型      说明   
  token  是      string  鉴权token



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "studentId": 2,
            "name": "test",
            "phone": "18661205610",
            "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg",
            "educationId": "1121",
            "sex": 0,
            "birthday": "1992-11-11T00:00:00.000Z",
            "districtId": "6",
            "schoolId": "1",
            "grade": null,
            "intro": null,
            "District": {
                "districtId": 6,
                "districtName": "921011102ceshi11"
            },
            "School": {
                "SchoolId": 1,
                "SchoolName": "未知"
            }
        },
        "reqId": "fdf0c81001",
        "errCode": 0,
        "errMsg": "ok"
    }

8. /v1/student/comments

描述

- 获取一个学生的评论和附议

请求方式

- GET

参数

   参数名      是否必须      类型       说明    
  token      是      string   鉴权token 
  limit      否       int    返回个数，默认20
  offset     否       int    偏移量，用于分页 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   List     object  结果集 

返回示例

    {
        "list": [
            {
                "commentId": 6,
                "proposalId": "1",
                "commenterType": 1,
                "criticsId": 1,
                "commentType": 1,
                "replyedId": null,
                "replyederId": null,
                "text": "test comment",
                "status": 1,
                "created_at": "2018-01-18T13:40:45.000Z",
                "updated_at": "2018-01-18T13:40:45.000Z",
                "Critics": {
                    "studentId": 1,
                    "name": null,
                    "phone": "18661205610",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            }
        ],
        "reqId": "94b3e41004",
        "errCode": 0,
        "errMsg": "ok"
    }



四、专家相关API

1. /v1/expert

描述

- 注册一个专家

请求方式

- POST

参数

     参数名    是否必须      类型     说明  
    phone    是      string  学生手机号
  validCode  是      String   验证码 
  password   是      String   密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "expertId": 2,
            "createTime": "2018-01-16T13:22:55.373Z"
        },
        "reqId": "54fcaf1001",
        "errCode": 0,
        "errMsg": "ok"
    }

2. /v1/expert/token

描述

- 专家账号登录，返回token及学生个人信息

请求方式

- POST

参数

    参数名     是否必须      类型     说明  
   phone     是      String  专家手机号
  password   是      String   密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "expertId": 2,
            "name": null,
            "phone": "18661205611",
            "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg",
            "company": null,
            "sex": 0,
            "duty": null,
            "intro": null,
            "domainId": '1|2',
            "expertType": 1,
            "districtId": 1,
            "schoolId": 1,
            "District": {
                "districtId": 1,
                "districtName": "未知"
            },
            "School": {
                "SchoolId": 1,
                "SchoolName": "未知"
            },
            "token": "c9d74a7106228dcbcf13c40f802995bc87314da4276700c468ace69d545c2a46f92c2ee8a4a536e51d9cd196f1a4fc640309f2bc23c235ae89f3e3e4a6773c92",
            "expires_time": 1518704793618
        },
        "reqId": "541b991002",
        "errCode": 0,
        "errMsg": "ok"
    }

3. /v1/expert

描述

- 修改一个专家个人信息

请求方式

- PUT

参数

     参数名        是否必须      类型                       说明                   
    token        是      string                  鉴权token                 
     name        否      string                    专家姓名                  
    avatar       否      string                     头像                   
     duty        否      string                     职务                   
   company       否      string                   公司，单位                  
     sex         否       Int                    1-男，2-女                 
   domainId      否      string  领域ids，字符串形式，以 \| 分隔每个领域id，如："\|12\|11\|10\|"
  districtId     否       Int                      学区id                  
   schoolId      否       Int                      学校id                  
  expertType     否       int                  类型，1-老师,2-专家              
    intro        否      String                    个人介绍                  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "expertId": 2
        },
        "reqId": "3515c41002",
        "errCode": 0,
        "errMsg": "ok"
    }

4. /v1/expert/action_forget_password

描述

- 专家账号忘记密码

请求方式

- PUT

参数

     参数名    是否必须      类型     说明  
  validCode  是      string   验证码 
    phone    是      string  专家手机号
  password   是      string   新密码 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "phone": 18700102254
        },
        "reqId": "bcf70c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

5. /v1/expert/action_update_password

描述

- 修改一个专家账号密码

请求方式

- PUT

参数

      参数名       是否必须      类型      说明   
     token       是      string  鉴权token
  oldPassword    是      string    旧密码  
  newPassword    是      string    新密码  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "phone": "18700102254"，
            "expertId": "2"
        },
        "reqId": "bcf70c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

6. /v1/expert/action_update_phone

描述

- 修改一个专家账号的手机号

请求方式

- PUT

参数

     参数名    是否必须      类型      说明   
    token    是      string  鉴权token
  validCode  是      string    验证码  
    phone    是      string   新手机号  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {//更换手机号后原token失效，因此直接返回了新的token
            "phone": "18661205611",
            "token": "ff7dad559839d5d28cba445894834840403004563c5408cc39d6d85480a4e096f390c680d55cda33dbb9c2bec80e493a65218112ae9741b533eecc34c0ea1c1f",
            "expires_time": 1518710597773
        },
        "reqId": "7032451005",
        "errCode": 0,
        "errMsg": "ok"
    }

7. /v1/expert

描述

- 获取一个专家账号个人信息

请求方式

- GET

参数

   参数名  是否必须      类型      说明   
  token  是      string  鉴权token



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "expertId": 1,
            "name": null,
            "phone": "18661205611",
            "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg",
            "company": null,
            "sex": 0,
            "duty": null,
            "intro": null,
            "domainId": "|1|2|",
            "expertType": 1,
            "districtId": 1,
            "schoolId": 1,
            "District": {
                "districtId": 1,
                "districtName": "未知"
            },
            "School": {
                "SchoolId": 1,
                "SchoolName": "未知"
            },
            "Domain": [
                {
                    "domainId": 1,
                    "domainName": "未知"
                },
                {
                    "domainId": 2,
                    "domainName": "IT"
                }
            ]
        },
        "reqId": "7436e21001",
        "errCode": 0,
        "errMsg": "ok"
    }

7. /v1/experts

描述

- 获取专家列表

请求方式

- GET

参数

    参数名     是否必须      类型       说明    
   token     是      string   鉴权token 
   limit     否       int    返回个数，默认20
   offset    否       int    偏移量，用于分页 
  domainId   否       int    根据领域id筛选 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     object  结果集 

返回示例

    {
        "list": [
            {
                "expertId": 1,
                "name": null,
                "phone": "18661205611",
                "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg",
                "company": null,
                "sex": 0,
                "duty": null,
                "intro": null,
                "domainId": "|1|",
                "expertType": 1,
                "districtId": 1,
                "schoolId": 1,
                "District": {
                    "districtId": 1,
                    "districtName": "未知"
                },
                "School": {
                    "SchoolId": 1,
                    "SchoolName": "未知"
                },
                "Domain": [
                    {
                        "domainId": 1,
                        "domainName": "未知"
                    }
                ],
                "commentCount": 0
            }
        ],
        "reqId": "329a501001",
        "errCode": 0,
        "errMsg": "ok"
    }

8. /v1/expert/comments

描述

- 获取一个专家的评论和附议

请求方式

- GET

参数

   参数名      是否必须      类型       说明    
  token      是      string   鉴权token 
  limit      否       int    返回个数，默认20
  offset     否       int    偏移量，用于分页 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   List     object  结果集 

返回示例

    {
        "list": [
            {
                "commentId": 2,
                "proposalId": "1",
                "commenterType": 2,//1-对提案评论，2-回复评论评论,3-附议，即点赞
                "criticsId": 1,
                "commentType": 2,
                "replyedId": 1,
                "replyederId": 1,
                "text": "test comment",
                "status": 1,
                "created_at": "2018-01-18T13:03:51.000Z",
                "updated_at": "2018-01-18T13:03:51.000Z",
                "Critics": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                },
                "Replyed": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            },
            {
                "commentId": 1,
                "proposalId": "1",
                "commenterType": 2,
                "criticsId": 1,
                "commentType": 1,
                "replyedId": null,
                "replyederId": null,
                "text": "test comment",
                "status": 1,
                "created_at": "2018-01-18T13:03:30.000Z",
                "updated_at": "2018-01-18T13:03:30.000Z",
                "Critics": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            },
            {
                "commentId": 3,
                "proposalId": "2",
                "commenterType": 2,
                "criticsId": 1,
                "commentType": 3,
                "replyedId": null,
                "replyederId": null,
                "text": "like",
                "status": 1,
                "created_at": "2018-01-01T00:00:00.000Z",
                "updated_at": "2018-01-01T00:00:00.000Z",
                "Critics": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            }
        ],
        "reqId": "d3b1c51002",
        "errCode": 0,
        "errMsg": "ok"
    }



五、提案相关API

1. /v1/proposal

描述

- 提交一个提案

请求方式

- POST

参数

    参数名     是否必须      类型                       说明                   
   token     是      string                  鉴权token                 
   title     是      string                     标题                   
  content    是      string                     内容                   
  adviser    是      string                    指导老师                  
  formism    是      string                    提案形式                  
  domainId   是      string  领域ids，字符串形式，以 \| 分隔每个领域id，如："\|12\|11\|10\|"
   status    否       int                   1-草稿，2-提交                



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "proposalId": 5,
            "createTime": "2018-01-17T13:15:26.543Z"
        },
        "reqId": "115c6d1001",
        "errCode": 0,
        "errMsg": "ok"
    }

2. /v1/proposal/:proposalId/action_likes

描述

- 点赞或者取消点赞

请求方式

- PUT

参数

      参数名       是否必须      类型        说明     
     token       是      string    鉴权token  
     type        是       Int    1-点赞，2-取消点赞
  :proposalId    是      string     提案id    



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "proposalId": 5
        },
        "reqId": "b45d9f1001",
        "errCode": 0,
        "errMsg": "ok"
    }

3. /v1/proposal/:proposalId

描述

- 根据提案id，更新提案

请求方式

- PUT

参数

      参数名       是否必须      类型                       说明                   
     token       是      string                  鉴权token                 
  :proposalId    是      string                    提案id                  
     title       否      string                     标题                   
    content      否      string                     内容                   
    adviser      否      string                    指导老师                  
    formism      否      string                    提案形式                  
   domainId      否      string  领域ids，字符串形式，以 \| 分隔每个领域id，如："12\|11\|10"
    status       否       int                   1-草稿，2-提交                



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "proposalId":js
        },
        "reqId": "3e5f7e1001",
        "errCode": 0,
        "errMsg": "ok"
    }

4. /v1/proposal/:proposalId

描述

- 根据提案id获取提案信息

请求方式

- GET

参数

      参数名       是否必须      类型      说明   
     token       是      string  鉴权token
  :proposalId    是      string   提案id  



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "proposalId": 5,
            "studentId": 1,
            "title": "提案测试update",
            "content": "提案内容999",
            "adviser": "建议人6",
            "formism": "形式3",
            "domainId": "|1|2|",
            "dismissal": ",",
            "created_at": "2018-01-17T13:15:26.000Z",
            "likes": 3,
            "status": 1,//1-草稿, 2-已提交 3-已审核 4-驳回 5-删除
            "Domain": [
                {
                    "domainId": 1,
                    "domainName": "未知"
                },
                {
                    "domainId": 2,
                    "domainName": "IT"
                }
            ]
        },
        "reqId": "3e5fc11002",
        "errCode": 0,
        "errMsg": "ok"
    }

5. /v1/proposals

描述

- 获取提案列表

请求方式

- GET

参数

   参数名      是否必须      类型               说明            
  token      是      string           鉴权token         
  offset     否       int            偏移量，用于分页         
  limit      否       int            返回个数，默认20        
   sort      否       int    1-创建时间排序（默认） 2-最新回复 3-附议数



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     object  结果集 

返回示例

    {
        "list": [
            {
                "proposalId": 5,
                "studentId": 1,
                "title": "提案测试update",
                "content": "提案内容999",
                "adviser": "建议人6",
                "formism": "形式3",
                "domainId": "|1|2|",
                "dismissal": ",",
                "created_at": "2018-01-17T13:15:26.000Z",
                "likes": 3,
                "status": 1,//1-草稿, 2-已提交 3-已审核 4-驳回 5-删除
                "Domain": [
                    {
                        "domainId": 1,
                        "domainName": "未知"
                    },
                    {
                        "domainId": 2,
                        "domainName": "IT"
                    }
                ],
                "commentCount": 0,//评论数
                "followCount": 0//关注数
            },
            {
                "proposalId": 4,
                "studentId": 1,
                "title": "提案测试3",
                "content": "提案内容3",
                "adviser": "建议人3",
                "formism": "形式3",
                "domainId": "1|2",
                "dismissal": null,
                "created_at": "2018-01-17T13:15:10.000Z",
                "likes": 0,
                "Domain": [
                    {
                        "domainId": 1,
                        "domainName": "未知"
                    },
                    {
                        "domainId": 2,
                        "domainName": "IT"
                    }
                ],
                "commentCount": 0
            },
            {
                "proposalId": 3,
                "studentId": 1,
                "title": "提案测试3",
                "content": "提案内容3",
                "adviser": "建议人3",
                "formism": "形式3",
                "domainId": "|1|2|",
                "dismissal": null,
                "created_at": "2018-01-17T13:07:14.000Z",
                "likes": 0,
                "status": 1,//1-草稿, 2-已提交 3-已审核 4-驳回 5-删除
                "Domain": [
                    {
                        "domainId": 1,
                        "domainName": "未知"
                    },
                    {
                        "domainId": 2,
                        "domainName": "IT"
                    }
                ],
                "commentCount": 0//评论数
            }
        ],
        "reqId": "1898d41001",
        "errCode": 0,
        "errMsg": "ok"
    }



六、评论相关API

1. /v1/comment

描述

- 提交一个评论

请求方式

- POST

参数

      参数名       是否必须      类型                       说明                   
     token       是      string                  鉴权token                 
  proposalId     是       int                    评论的提案id                 
   replyedId     否       int                    被回复的评论id                
  replyederId    否       int                  被回复的评论的人的id               
     text        是      string                    评论内容                  
  commentType    否       int    1-对提案评论，2-回复评论评论 ,评论类型，默认为1，为2时，replyedId，replyederId必传
    status       否       int                   1-提交，默认为1                



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "commentId": 4,
            "createTime": "2018-01-18T12:33:01.900Z"
        },
        "reqId": "b193fd1001",
        "errCode": 0,
        "errMsg": "ok"
    }

2. /v1/comments

描述

- 根据一个提案id获取评论

请求方式

- GET

参数

     参数名        是否必须      类型       说明    
    offset       否       int    偏移量，用于分页 
    limit        否       int    返回个数，默认20
    token        是      string   鉴权token 
  proposalId     是       int     评论的提案id 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     Array   学区列表

返回示例

    {
        "list": [
            {
                "commentId": 2,
                "proposalId": "1",
                "commenterType": 2,
                "criticsId": 1,
                "commentType": 2,//1-对提案评论，2-回复评论评论
                "replyedId": 1,
                "replyederId": 1,
                "text": "test comment",
                "status": 1,
                "created_at": "2018-01-18T13:03:51.000Z",
                "updated_at": "2018-01-18T13:03:51.000Z",
                "Critics": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                },
                "Replyed": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            },
            {
                "commentId": 1,
                "proposalId": "1",
                "commenterType": 2,
                "criticsId": 1,
                "commentType": 1,
                "replyedId": null,
                "replyederId": null,
                "text": "test comment",
                "status": 1,
                "created_at": "2018-01-18T13:03:30.000Z",
                "updated_at": "2018-01-18T13:03:30.000Z",
                "Critics": {
                    "expertId": 1,
                    "name": null,
                    "phone": "18661205611",
                    "avatar": "http://ohozpmsy1.bkt.clouddn.com/timg.jpeg"
                }
            }
        ],
        "reqId": "769bac1001",
        "errCode": 0,
        "errMsg": "ok"
    }

七、关注相关API 

1. /v1/follows

描述

- 提交一个关注

请求方式

- POST

参数

     参数名        是否必须      类型                 说明             
    token        是      string            鉴权token           
  proposalId     是       int                提案id            
     text        是      string               内容             
  followType     是       int    关注方式，1-仅关注，2-作为提案关注 3-作为提案素材



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
  result    object  结果集 

返回示例

    {
        "result": {
            "createTime": "2018-01-18T14:00:44.662Z"
        },
        "reqId": "5eb88c1001",
        "errCode": 0,
        "errMsg": "ok"
    }

5. /v1/follows

描述

- 获取提案列表

请求方式

- GET

参数

     参数名        是否必须      类型       说明    
    token        是      string   鉴权token 
  proposalId     是       int      提案id   
    limit        否       int    返回个数，默认20
    offset       否       int    偏移量，用于分页 



返回参数说明

    参数名       类型     说明 
   reqId    string  请求id
  errCode    Int    错误码 
  errMsg    string  错误信息
   list     object  结果集 

返回示例

    {
        "list": [
            {
                "followId": 1,
                "proposalId": "1",
                "expertId": 1,
                "text": "test follow",
                "followType": "1",
                "status": 1,
                "created_at": "2018-01-18T14:00:44.000Z",
                "updated_at": "2018-01-18T14:00:44.000Z"
            }
        ],
        "reqId": "98b9d01001",
        "errCode": 0,
        "errMsg": "ok"
    }


