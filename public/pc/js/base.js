$(function () {
  if ($.Fg.getSession($.Fg.keys.USERTYPE) == 1) {
    $(".j-expert-hidden").removeClass("hidden");
  } else {
    $(".j-student-hidden").removeClass("hidden");
  }



  $("#logout").on("click", function () {
    $.Fg.clearSession();
    location.href = "index.html";
  });

  $("#back").click(function () {
    var type = $.Fg.getSession($.Fg.keys.USERTYPE);
    if (type == 1) {
      location.href = "infoStu.html";
    } else {
      location.href = "infoExp.html";
    }
  });
});

$.Fg = {
  checkInfo: function () {
    var user = $.Fg.getSession($.Fg.keys.USERINFO);
    var type = $.Fg.getSession($.Fg.keys.USERTYPE);
    if (!user) {
      location.href = "index.html";
    }
    var obj = {
      "1": [
        "avatar",
        "sex",
        "birthday",
        "districtId",
        "schoolId",
        "phone",
        "name",
        "grade"
      ],
      "2": ["name", "avatar", "company", "duty", "domainId", "sex"]
    };
    $.each(obj[type], function (k, v) {
      if (!!!user[v]) {
        location.href = type == 1 ? "infoStu.html" : "infoExp.html";
      }
    });
  },
  keys: {
    TOKEN: "FG_TOKEN",
    USERINFO: "FG_USER_INFO",
    USERTYPE: "FG_USER_TYPE" // /* 1: 学生 2: 专家 */
  },
  QINIU_URL: "http://p35rxh2xg.bkt.clouddn.com/",
  ajax: function (url, opt) {
    var _data;
    if (opt.isLogin) {
      _data = opt.data;
    } else {
      if ($.Fg.getSession($.Fg.keys.TOKEN) == "") {
        location.href = "index.html";
        return false;
      } else {
        _data = $.extend(true, {}, opt.data, {
          token: $.Fg.getSession($.Fg.keys.TOKEN)
        });
      }
    }
    $.ajax({
      // url: "http://mnzx.fungoedu.com:7005/v1/" + url,
      // url: "http://127.0.0.1:7005/v1/" + url,
      url: "http://mnzx.fungoedu.com:7006/v1/" + url,
      method: opt.method || "GET",
      dataType: "json",
      data: _data,
      success: function (data) {
        if (data.errCode === 0) {
          opt.success(data);
        } else {
          if (
            typeof opt.error !== "undefined" &&
            typeof opt.error === "function"
          ) {
            opt.error(data);
          } else {
            alert(data.errMsg || "请求数据出错");
          }
        }
      },
      error: function () {
        alert("服务器错误");
      }
    });
  },
  getSession: function (key) {
    var _str = sessionStorage.getItem(key);
    if (_str) {
      return JSON.parse(_str);
    } else {
      return "";
    }
  },
  setSession: function (key, val) {
    sessionStorage.setItem(key, JSON.stringify(val));
  },
  clearSession: function () {
    sessionStorage.clear();
  },
  //获取url参数
  getParam: function (name, def) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
      return unescape(r[2]);
    } else {
      return def;
    }
  },
  alert: function (msg) {
    alert(msg);
  },
  getUrlByType: function (type) {
    return $.Fg.getSession($.Fg.keys.USERTYPE) == 2 ? "expert" : "student";
  },
  formatJson: function (str) {
    return str.replace(/\n/g, "<br />");
  },
  formatD: function (str, def) {
    return moment(str).format(def || "YYYY-MM-DD HH:mm:ss");
  }
};