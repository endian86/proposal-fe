/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//获取url参数
function getParam(name, def) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    } else {
        return def;
    }
}
// <<<<<<< HEAD
// //1-对提案评论，2-回复评论评论,3-附议，即点赞
// var commentType = {"1":"评论","2":"回复","3":"附议"};

// function FgAjax(url, opt) {
//     var _data = {};
//     if (localStorage.getItem("token")) {
//         _data = $.extend(true, {}, {token: localStorage.getItem("token")}, opt.data);
// =======
//1-对提案评论，2-回复评论评论,3-附议，即附议
var commentType = {
    "1": "评论",
    "2": "回复",
    "3": "附议"
};

function FgAjax(url, opt) {
    var _data = {};
    if (sessionStorage.getItem("token")) {
        _data = $.extend(true, {}, {
            token: sessionStorage.getItem("token")
        }, opt.data);
    } else {
        _data = opt.data
    }
    $.ajax({
        type: opt.method || "GET",
        url: baseAPi + url,
        dataType: "json",
        data: _data,
        success: function (respJson) {
            if (respJson.errCode == 0) {
                opt.success(respJson);
            } else {
                if (respJson.errCode == 1062) {
                    weui.alert('请勿重复点赞');
                    return;
                }
                if (opt.errCode) {
                    opt.errCode(respJson);
                } else {
                    weui.alert(respJson.errMsg);
                }
            }
        },
        error: function (err) {
            console.log(err);

            // alert("服务器异常或者网络异常");
        }
    });
}


function uploadImg(f, token, key) {
    var Qiniu_UploadUrl = "http://up.qiniu.com";
    //var Qiniu_UploadUrl = "https://resource.beilezx.com/";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', Qiniu_UploadUrl, true);
    var formData = new FormData();
    if (key !== null && key !== undefined) {
        formData.append('key', key);
    }
    formData.append('token', token);
    formData.append('file', f);
    xhr.onreadystatechange = function (response) {
        if (xhr.readyState == 4 && xhr.status == 200 && xhr.responseText != "") {
            var blkRet = JSON.parse(xhr.responseText);
            $("#dialog").html("复制连接直接访问_:https://resource.beilezx.com/" + blkRet.key).dialog();
        } else if (xhr.status != 200 && xhr.responseText) {
            concole.log("服务传输异常!!");
        }
    };
    xhr.send(formData);
}

function countDown($obj, end) {
    var _s = 60;
    var _time = setInterval(function () {
        if (_s >= 1) {
            $obj.text(_s + 's');
            _s--;
        } else {
            $obj.text('重新获取');
            if (end) {
                end();
            }
            clearInterval(_time);
        }
    }, 1000);
}

function formatD(str, def) {
    return moment(str).format(def || "YYYY-MM-DD HH:mm:ss")
}