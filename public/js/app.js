var baseAPi = 'http://39.106.210.145:7006'
// var baseAPi = 'http://127.0.0.1:7005'
// 18661205610的密码是123456

// 登陆
function LoginSubmitForm(type) {
    var formString = $("#infoform").serialize()
    var api = '/v1/student/token'
    // if (type === 'expert') {

    // }
    loginAjax(api, formString, function (res) {
        if (res.errCode == 0) {
            setLoginData(res, '')
        } else {
            api = '/v1/expert/token'
            loginAjax(api, formString, function (res) {
                if (res.errCode == 0) {
                    setLoginData(res, 'expert')
                } else {
                    alert('请输入正确的手机号和密码')
                }
            })
        }
    })
    // fetch(api, formString, 'POST').then(function (respJson) {

    // }, function (data) {
    //     alert('请输入正确的手机号和密码')
    // })
}

function setLoginData(respJson, type) {
    var token = respJson.result.token
    var phone = document.getElementById("phone").value

    sessionStorage.setItem("token", token);
    sessionStorage.setItem("phone", phone);
    sessionStorage.setItem("userId", respJson.result.expertId || respJson.result.studentId);
    sessionStorage.setItem("user_type", (type == "expert" ? "expert" : 'student'));
    var userData = {
        name: respJson.result.name,
        grade: respJson.result.grade,
        duty: respJson.result.duty,
        domainId: respJson.result.domainId,
        company: respJson.result.company,
    }

    sessionStorage.setItem(phone, JSON.stringify(userData));
    // 跳转到plist
    self.location = "/plist"
}

function loginAjax(url, data, callback) {
    $.ajax({
        type: 'post',
        url: baseAPi + url,
        data: data,
        success: function (respJson) {
            callback(respJson);
            // 也可以得到服务器返回的整个json
            // console.log(JSON.stringify(respJson))
            // 可以通过json获取里面的数据
            // console.log(respJson.reqId)
            // if (respJson.errCode != 0) {
            //     // alert(respJson.errMsg)
            //     // self.location = "/login"
            //     def.reject(respJson)
            //     return
            // }
            // def.resolve(respJson);
        }
    });
}
//获取提案列表
function getExpectList(v) {
    var api = '/v1/proposals?limit=200'
    if (v) {
        api = api + "&sort=" + v
    } else {
        api = api + "&sort=3"
    }
    fetch(api, 'GET').then(function (data) {
        var htmlStr = '<figure>'
        $.each(data.list, function (index, value) {
            var biaqian = ''
            $.each(value.Domain, function (index, dma) {
                if (dma.domainName && dma.domainName != "未知") {
                    biaqian += '<a>' + dma.domainName + '</a>'
                }
            })
            if (value.Student.School && value.Student.School.schoolName && value.Student.School.schoolName != '未知') {
                biaqian = biaqian + '<a>' + value.Student.School.schoolName + '</a>'
            }
            if (value.adviser) {
                biaqian = biaqian + '<a>' + value.adviser + '</a>'
            }
            // <span style="color:blue;margin-bottom:0rem;">展开全文</span>
            console.log(value.content)
            value.content = value.content.replaceAll("\n", "</p><p>")
            htmlStr = htmlStr + ' <div class="Sec_content_two_con_list"><div class="Sec_content_two_con_list_left"><img src="' + value.Student.avatar + '"></div><div class="Sec_content_two_con_list_right"> <p class="Sec_content_two_con_list_ni">' + value.Student.name + '</p> <div class="Sec_content_two_con_list_right_a">' + biaqian + '</div><h4 style="margin-top:0rem;">' + value.title + '</h4><p>' + value.content + '</p><span>发布时间：' + Format(new Date(value.created_at), 'yyyy-MM-dd hh:mm:ss') + '</span> <div id="plist_' + value.proposalId + '"></div><div class="Sec_content_two_con_list_bottom" style="width:100%;"> <div  onClick="likes(' + value.proposalId + ')"><img src="img/icon14.png"><p id="' + value.proposalId + 'like">' + value.likes + '</p> </div> <div   onClick="pl(' + value.proposalId + ')"><img src="img/icon15.png"><p id="' + value.proposalId + 'pl">' + value.commentCount + '</p> </div>   </div></div> </div>'
        })
        $('#list_Ex').eq(0).html(htmlStr + '</figure>');
    })
}

function likes(id) {
    fetch('/v1/proposal/' + id + '/action_likes', {
        type: 1,
        token: sessionStorage.getItem("token")
    }, 'PUT').then(function (data) {
        if (data.result) {
            alert("附议成功")
            var likes = $('#' + id + 'like').text()
            $('#' + id + 'like').text(++likes)
        }
    })
}

function pl(id) {
    // plist_' + value.proposalId + '
    fetch('/v1/comments?limit=10&proposalId=' + id, 'GET').then(function (data) {
        var data = data.list
        var str = '<div><ul>'
        $.each(data, function (index, value) {
            str = str + '<li>' + value.Critics.name + '   （' + Format(new Date(value.created_at), 'yyyy-MM-dd hh:mm') + '）' + value.text + '</li>'
        })
        str = str + '<li><span><textarea id="' + id + 'pub" placeholder="请输入评论..."   type="text"></textarea><a  href="####" onClick="pub(' + id + ')" class="ubp" >发布评论</a><span></li><li><span></span><li></ul ></div>'
        $('#plist_' + id).eq(0).html(str);
    })
}

function pub(id) {
    fetch('/v1/comment', {
        proposalId: id,
        text: $('#' + id + 'pub').val(),
        commentType: 1,
        token: sessionStorage.getItem("token")
    }, 'POST').then(function (data) {
        alert('添加评论成功')
        pl(id)
        var pls = $('#' + id + 'pl').text()
        $('#' + id + 'pl').text(++pls)
    })
}

var fetch = function (url, data, method) {
    var def = $.Deferred();
    // if (data && method) {
    //     let s = []
    //     for (let key in data) {
    //         if (data.hasOwnProperty(key)) {
    //             s.push(key + '=' + data[key])
    //         }
    //     }
    //     data = s.join('&')
    // }
    if (!method) {
        method = data
    }
    if (sessionStorage.getItem("token")) {
        if (url.indexOf('?') > -1) {
            url = url + '&token=' + sessionStorage.getItem("token")
        } else {
            url = url + '?token=' + sessionStorage.getItem("token")
        }
    }
    $.ajax({
        type: method,
        url: baseAPi + url,
        data: data,
        success: function (respJson) {
            // 也可以得到服务器返回的整个json
            // console.log(JSON.stringify(respJson))
            // 可以通过json获取里面的数据
            // console.log(respJson.reqId)
            if (respJson.errCode != 0) {
                // alert(respJson.errMsg)
                // self.location = "/login"
                def.reject(respJson)
                return
            }
            def.resolve(respJson);
        }
    });
    return def
}


function Format(now, mask) {
    var d = now;
    var zeroize = function (value, length) {
        if (!length) length = 2;
        value = String(value);
        for (var i = 0, zeros = ''; i < (length - value.length); i++) {
            zeros += '0';
        }
        return zeros + value;
    };

    return mask.replace(/"[^"]*"|'[^']*'|\b(?:d{1,4}|m{1,4}|yy(?:yy)?|([hHMstT])\1?|[lLZ])\b/g, function ($0) {
        switch ($0) {
            case 'd':
                return d.getDate();
            case 'dd':
                return zeroize(d.getDate());
            case 'ddd':
                return ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'][d.getDay()];
            case 'dddd':
                return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][d.getDay()];
            case 'M':
                return d.getMonth() + 1;
            case 'MM':
                return zeroize(d.getMonth() + 1);
            case 'MMM':
                return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][d.getMonth()];
            case 'MMMM':
                return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][d.getMonth()];
            case 'yy':
                return String(d.getFullYear()).substr(2);
            case 'yyyy':
                return d.getFullYear();
            case 'h':
                return d.getHours() % 12 || 12;
            case 'hh':
                return zeroize(d.getHours() % 12 || 12);
            case 'H':
                return d.getHours();
            case 'HH':
                return zeroize(d.getHours());
            case 'm':
                return d.getMinutes();
            case 'mm':
                return zeroize(d.getMinutes());
            case 's':
                return d.getSeconds();
            case 'ss':
                return zeroize(d.getSeconds());
            case 'l':
                return zeroize(d.getMilliseconds(), 3);
            case 'L':
                var m = d.getMilliseconds();
                if (m > 99) m = Math.round(m / 10);
                return zeroize(m);
            case 'tt':
                return d.getHours() < 12 ? 'am' : 'pm';
            case 'TT':
                return d.getHours() < 12 ? 'AM' : 'PM';
            case 'Z':
                return d.toUTCString().match(/[A-Z]+$/);
                // Return quoted strings with the surrounding quotes removed
            default:
                return $0.substr(1, $0.length - 2);
        }
    });
};
String.prototype.replaceAll = function (s1, s2) {
    return this.replace(new RegExp(s1, "gm"), s2);
}