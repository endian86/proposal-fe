var express = require("express");
var router = express.Router();

// router.get('/pc', function (req, res, next) {
//   res.render('registered', { title: 'Express' });
// });

/* GET home page. */
router.get("/reg", function (req, res, next) {
  res.render("registered", {
    title: "Express"
  });
});
//密码验证
router.get("/check", function (req, res, next) {
  res.render("check", {
    title: "Express"
  });
  check;
});
//专家详情
router.get("/expert_info", function (req, res, next) {
  res.render("expert_info", {
    title: "Express"
  });
});
// 个人中心
router.get("/personal", function (req, res, next) {
  res.render("personal", {
    title: "Express"
  });
});
// 个人资料
router.get("/personal_eidt", function (req, res, next) {
  res.render("personal_eidt", {
    title: "Express"
  });
});
// 专家个人资料
router.get("/personal_eidt_exp", function (req, res, next) {
  res.render("personal_eidt_exp", {
    title: "Express"
  });
});
// 修改密码（忘记密码）
router.get("/updatePass", function (req, res, next) {
  res.render("updatePass", {
    title: "Express"
  });
});
// 新手机绑定
router.get("/updatePass_bd", function (req, res, next) {
  res.render("updatePass_bd", {
    title: "Express"
  });
});
// 修改密码（个人资料中的修改密码）
router.get("/updatePass_dl", function (req, res, next) {
  res.render("updatePass_dl", {
    title: "Express"
  });
});
// 评论
router.get("/comment", function (req, res, next) {
  res.render("comment", {
    title: "Express"
  });
});
// 关注专家
router.get("/follow", function (req, res, next) {
  res.render("follow", {
    title: "Express"
  });
});

router.get("/ereg", function (req, res, next) {
  res.render("Expertsregistered", {
    title: "Express"
  });
});

router.get("/einfo", function (req, res, next) {
  res.render("expert_reg", {
    title: "Express"
  });
});

router.get("/", function (req, res, next) {
  res.render("simulation", {
    title: "Express"
  });
});

router.get("/sreg", function (req, res, next) {
  res.render("Studentregistration", {
    title: "Express"
  });
});

router.get("/sinfo", function (req, res, next) {
  res.render("student_reg", {
    title: "Express"
  });
});

router.get("/newp", function (req, res, next) {
  res.render("case_fb", {
    title: "Express"
  });
});

router.get("/login", function (req, res, next) {
  res.render("login", {
    title: "登录"
  });
});
router.get("/details", function (req, res, next) {
  res.render("details", {
    title: "详情"
  });
});
router.get("/nlist", function (req, res, next) {
  res.render("nlist", {
    title: "新闻中心"
  });
});
router.get("/llist", function (req, res, next) {
  res.render("llist", {
    title: "资料库"
  });
});
router.get("/plist", function (req, res, next) {
  var data = {
    title: "提案列表"
  };
  res.render("Secondedbyreplyfour", data);
});
router.get("/proposalDetail", function (req, res, next) {
  res.render("proposalDetail", {
    title: "提案详情"
  });
});

module.exports = router;